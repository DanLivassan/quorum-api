1. Discuss your solution’s time complexity. What tradeoffs did you make?
- The time complexy is close to O(n). Because the loop inside the processer.py file running one time but we have to search other entities at the first time that entity appears through the loop:
```
        for vote_result in self.vote_results:
            leg_id = vote_result.legislator_id
            vote_id = vote_result.vote_id
            if leg_id not in leg_id_yes:
                leg_id_yes[leg_id]=0
                leg_id_no[leg_id]=0
                legislator = next((l for l in self.legislators if l.id == leg_id), None)
                leg_name[leg_id] = legislator.name
```

The tradeoff that i choose was performance instead of code structure

2. How would you change your solution to account for future columns that might be
requested, such as “Bill Voted On Date” or “Co-Sponsors”?
You have to add that in the Bills dictionaries inside processers

3. How would you change your solution if instead of receiving CSVs of data, you were given a
list of legislators or bills that you should generate a CSV for?
I have created a interface (DataSupplier) that the processer depends of. Also i've created a interface in order to export the data (DataExporter) using the dependency injection principle

4. How long did you spend working on the assignment?
1h:40m

Notes:

I have used the pydantic library in order to be sure that the data will be passed thorugh our modules

***Install all dependencies***

`pip install -r requirements.txt`

***To run the script you should type***

`python main.py` at main folder

***To run the tests you should type***

`python -m unittest discover .`
