from interfaces import DataExporter
import csv

class CSVDataExporter(DataExporter):
    def export(self, legislators_processed, bills_processed):
        keys = (legislators_processed[0].keys())
        with open('legislators_support_oppose_count.csv', 'w', newline='') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(legislators_processed)
        keys = (bills_processed[0].keys())
        with open('bills.csv', 'w', newline='') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(bills_processed)
