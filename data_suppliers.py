from interfaces import DataSupplier
import csv
from models import Bill, Legislator, Vote, VoteResult

MAP_NAME_INTO_MODEL = {
    'bill': Bill,
    'legislator': Legislator,
    'vote': Vote,
    'vote_result': VoteResult,
}
class CSVDataSupplier(DataSupplier):
    def get_data(self, *args, **kwargs):
        path = kwargs['path']
        model_name = kwargs['model']
        data = []
        with open(path, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                data.append(self.transform_data(row, model_name))
        return data

    def transform_data(self,row, model_name):
        if model_name not in MAP_NAME_INTO_MODEL:
            raise Exception('The model name is not mapped')
        return MAP_NAME_INTO_MODEL[model_name](**row)