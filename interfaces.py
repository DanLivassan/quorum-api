from typing import Any
class DataSupplier:
    def get_data()-> Any:
        raise NotImplemented
    def transform_data() -> Any:
        raise NotImplemented

class DataExporter:
    def export(self, legislators_processed, bills_processed)->Any:
        raise NotImplemented