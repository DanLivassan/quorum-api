from data_suppliers import CSVDataSupplier
from data_exporter import CSVDataExporter
from processer import VoteProcesser

if __name__ == '__main__':

    processer = VoteProcesser(data_supplier=CSVDataSupplier(), data_exporter=CSVDataExporter())
    processer.get_bills(path='./data/bills.csv', model='bill')
    processer.get_legislators(path='./data/legislators.csv', model='legislator')
    processer.get_votes(path='./data/votes.csv', model='vote')
    processer.get_votes_results(path='./data/vote_results.csv', model='vote_result')
    processer.process_and_export()
    