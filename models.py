
from pydantic import BaseModel, ValidationError, validator

class Legislator(BaseModel):
    id: str
    name: str

class Bill(BaseModel):
    id:str
    title:str
    sponsor_id:str

class Vote(BaseModel):
    id:str
    bill_id:str

class VoteResult(BaseModel):
    id:str
    legislator_id:str
    vote_id:str
    vote_type:str

    @validator('vote_type')
    def vote_type_must_be_valid(cls, v):
        if v not in ['1', '2']:
            raise ValidationError('Invalid vote option')
        return v.title()