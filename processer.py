from interfaces import DataSupplier, DataExporter
from typing import Any, List
from models import Bill, Legislator, Vote, VoteResult
from collections import defaultdict

class VoteProcesser:
    
    data_exporter: DataExporter
    data_supplier: DataSupplier
    bills:  List[Bill]
    legislators:  List[Legislator]
    votes:  List[Vote]
    vote_results:  List[VoteResult]

    def __init__(self, data_supplier, data_exporter) -> None:
        self.data_supplier = data_supplier
        self.data_exporter = data_exporter
    def get_bills(self, *args, **kwargs)->List[Any]:
        self.bills = self.data_supplier.get_data(**kwargs)
        return self.bills
    def get_legislators(self, *args, **kwargs)->List[Any]:
        self.legislators = self.data_supplier.get_data(**kwargs)
        return self.legislators
    def get_votes(self, *args, **kwargs)->List[Any]:
        self.votes = self.data_supplier.get_data(**kwargs)
        return self.votes
    def get_votes_results(self, *args, **kwargs)->List[Any]:
        self.vote_results = self.data_supplier.get_data(**kwargs)
        return self.vote_results
    
    def process_and_export(self):
        if any([self.legislators, self.bills, self.vote_results, self.votes]) is None:
            raise Exception("Error: Couldn't process the data. Some missing data")
         # Legislators dictionaries
        leg_id_yes = defaultdict()
        leg_id_no = defaultdict()
        leg_name = defaultdict()
        
        # Bills dictionaries
        vote_id_yes = defaultdict()
        vote_id_no = defaultdict()
        bill_title = defaultdict()
        bill_sponsor_name = defaultdict()
        map_vote_bill = defaultdict()


        for vote_result in self.vote_results:
            leg_id = vote_result.legislator_id
            vote_id = vote_result.vote_id
            
            if leg_id not in leg_id_yes:
                leg_id_yes[leg_id]=0
                leg_id_no[leg_id]=0
                legislator = next((l for l in self.legislators if l.id == leg_id), None)
                leg_name[leg_id] = legislator.name

            if vote_id not in vote_id_yes:
                vote_id_yes[vote_id]=0
                vote_id_no[vote_id]=0
                vote = next((v for v in self.votes if v.id == vote_id), None)
                bill = next((b for b in self.bills if b.id == vote.bill_id), None)
                map_vote_bill[vote_id] = bill.id

                bill_title[bill.id] = bill.title
                bill_leg = next((l for l in self.legislators if l.id == bill.sponsor_id), None)
                bill_sponsor_name[bill.id] = bill_leg.name if bill_leg else 'Unknown'

            if vote_result.vote_type == '1':
                leg_id_yes[leg_id]+=1
                vote_id_yes[vote_id]+=1
            if vote_result.vote_type == '2':
                leg_id_no[leg_id]+=1
                vote_id_no[vote_id]+=1
            
        legislators_processed = []
        bills_processed = []

        for key in leg_name:
            legislators_processed.append({
                'id': int(key), 
                'name':leg_name[key],
                'num_supported_bills':leg_id_yes[key],
                'num_opposed_bills':leg_id_no[key],
            })
        
        for key in vote_id_yes:
            id = map_vote_bill[key]
            bills_processed.append({
                'id': int(id), 
                'title':bill_title[id],
                'supporter_count':vote_id_yes[key],
                'opposer_count':vote_id_no[key],
                'primary_sponsor':bill_sponsor_name[id]
            })
        self.export(legislators_processed, bills_processed)
        return legislators_processed, bills_processed

    def export(self,legislators_processed, bills_processed):
        self.data_exporter.export(legislators_processed, bills_processed)
