from models import VoteResult, Legislator
from pydantic.error_wrappers import ValidationError
import unittest

class TestVoteResult(unittest.TestCase):

    def test_if_throws_with_empty_params(self):
        with self.assertRaises(ValidationError) as context:
            VoteResult()
        assert('id' in str(context.exception))
    def test_if_throws_with_wrong_vote_type(self):
        with self.assertRaises(ValidationError) as _:
            VoteResult(id='12345', legislator_id='12345', vote_id='12345', vote_type='3')

class TestLegislator(unittest.TestCase):
    def test_if_throws_with_empty_params(self):
        with self.assertRaises(ValidationError) as context:
            Legislator()
        assert('id' in str(context.exception))
        assert('name' in str(context.exception))


if __name__ == '__main__':
    unittest.main()